<?php
$url = urldecode($_GET["url"]);
if(isset($url) && preg_match("/^https?:/",$url)){
    $ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	$data = curl_exec($ch);
	curl_close($ch);
	echo $data;
}else{
    echo "ERROR!";
}